package com.mortgage.servicecalculator.sercice;

import com.mortgage.servicecalculator.error.exception.AppException;
import com.mortgage.servicecalculator.mapper.AmountMapper;
import com.mortgage.servicecalculator.model.bo.Loan;
import io.reactivex.Single;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.mortgage.servicecalculator.constants.GlobalConstants.*;
import static com.mortgage.servicecalculator.error.exception.TechnicalCode.API_BAD_REQUEST;

@Service
public class LoanService {

    private static final Logger LOGGER = LoggerFactory.getLogger(Loan.class);

    public LoanService() {
    }

    private static AppException throwErrorWhenTooManyUnknown() {
        String errorMsg = "Too many unknowns to make the calculation";
        LOGGER.debug(errorMsg);
        return new AppException(errorMsg, API_BAD_REQUEST);
    }

    public Single<Loan> calculateLoan(Loan loan) {
        return Single.just(loan)
                .map(p -> Optional.ofNullable(loan)
                        .orElseThrow(LoanService::throwErrorWhenTooManyUnknown))
                .map(this::processHomeValue)
                .map(this::processInitialAmount)
                .map(this::processLoan);
    }

    private <T> boolean checkNullPer3(T a, T b) {
        return Optional.of(true)
                .filter(p -> a != null)
                .filter(p -> b != null)
                .orElse(false);
    }

    private Loan processHomeValue(Loan loan) {
        return loan.getHomeValue() == null ? Optional.ofNullable(loan)
                .filter(p -> checkNullPer3(p.getLoanAmount(), p.getInitialCapital()))
                .map(p -> p.getLoanAmount().multiply(1.00 - FEE_RATE))
                .map(p -> p.subtract(loan.getInitialCapital()))
                .map(p -> p.add(AmountMapper.map(CHARGE_LEVEL).multiply(CHARGE_RATE)))
                .map(p -> p.divide(1.00 + CHARGE_RATE))
                .map(loan::setHomeValue)
                .orElseThrow(LoanService::throwErrorWhenTooManyUnknown) :
                loan;
    }

    private Loan processLoan(Loan loan) {
        return loan.getLoanAmount() == null ? Optional.ofNullable(loan)
                .filter(p -> checkNullPer3(p.getHomeValue(), p.getInitialCapital()))
                .map(p -> p.getHomeValue().multiply(1.00 + CHARGE_RATE))
                .map(p -> p.subtract(AmountMapper.map(CHARGE_LEVEL).multiply(CHARGE_RATE)))
                .map(p -> p.add(loan.getInitialCapital()))
                .map(p -> p.divide(1.00 - CHARGE_RATE))
                .map(loan::setLoanAmount)
                .orElseThrow(LoanService::throwErrorWhenTooManyUnknown) :
                loan;
    }

    private Loan processInitialAmount(Loan loan) {
        return loan.getInitialCapital() == null ? Optional.ofNullable(loan)
                .filter(p -> checkNullPer3(p.getHomeValue(), p.getLoanAmount()))
                .map(p -> p.getHomeValue().multiply(1.00 + CHARGE_RATE))
                .map(p -> p.subtract(AmountMapper.map(CHARGE_LEVEL).multiply(CHARGE_RATE)))
                .map(p -> p.add(loan.getLoanAmount().multiply(1.00 - FEE_RATE)))
                .map(loan::setInitialCapital)
                .orElseThrow(LoanService::throwErrorWhenTooManyUnknown):
                loan;
    }
}
