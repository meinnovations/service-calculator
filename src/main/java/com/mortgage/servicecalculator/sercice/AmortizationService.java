package com.mortgage.servicecalculator.sercice;

import com.mortgage.servicecalculator.error.exception.AppException;
import com.mortgage.servicecalculator.mapper.AmountMapper;
import com.mortgage.servicecalculator.mapper.NumberMapper;
import com.mortgage.servicecalculator.model.bo.AmortizationInput;
import com.mortgage.servicecalculator.model.bo.AmortizationTable;
import com.mortgage.servicecalculator.model.bo.LoanAmortization;
import io.reactivex.Single;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.money.MonetaryAmount;
import javax.money.NumberSupplier;
import javax.money.NumberValue;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.mortgage.servicecalculator.error.exception.TechnicalCode.API_BAD_REQUEST;
import static java.lang.String.format;

@Service
public class AmortizationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoanAmortization.class);

    public AmortizationService() {
    }

    private static AppException throwErrorWhenInputMissing(String input) {
        String errorMsg = format("The input is missing to calculate %s", input);
        LOGGER.debug(errorMsg);
        return new AppException(errorMsg, API_BAD_REQUEST);
    }

    public Single<AmortizationTable> calculateLoanAmortization(AmortizationInput amortizationInput) {
        return Single.just(amortizationInput).map(p -> p.setMonthlyInterestRate(
                processMonthlyInterestRate(p.getAnnualInterestRate())))
                .map(this::processMonthlyAmount)
                .map(p -> new AmortizationTable()
                        .setAmortizationInput(p)
                        .setAmortizations(processAmortizationList(p)));
    }

    public BigDecimal processMonthlyInterestRate(BigDecimal annalInterestRate) {
        return Optional.ofNullable(annalInterestRate).map(NumberMapper::mapDoubleToBigDecimal)
                .map(p -> p + 1)
                .map(p -> Math.pow(p, 1.00 / 12.00)).map(p -> {
                    return p - 1.00;
                }).map(p -> NumberMapper.mapDoubleToBigDecimal(p, 5))
                .orElse(null);
    }

    public AmortizationInput processMonthlyAmount(AmortizationInput amortizationInput) {
        /*
        double capital = Optional.ofNullable(amortizationInput)
                .map(AmortizationInput::getLoanAmount)
                .map(NumberSupplier::getNumber)
                .map(NumberValue::doubleValueExact)
                .orElseThrow(() -> throwErrorWhenInputMissing("loanAmount or the all input"));
        double monthlyRate = Optional.ofNullable(amortizationInput)
                .map(AmortizationInput::getMonthlyInterestRate)
                .map(BigDecimal::doubleValue)
                .orElseThrow(() -> throwErrorWhenInputMissing("InterestRate or the oll input"));
        double duration = Optional.ofNullable(amortizationInput)
                .map(AmortizationInput::getLoanTerm)
                .orElseThrow(() -> throwErrorWhenInputMissing("InterestRate or the oll input"));

        return Optional.of(duration)
                .map(p -> Math.pow(1.00 + monthlyRate, p))
                .map(p -> p / (p - 1))
                .map(p -> capital * monthlyRate * p)
                .map(Object::toString)
                .map(AmountMapper::map)
                .map(amortizationInput::setMonthlyAmount)
                .get();
                */
        return Optional.ofNullable(amortizationInput)
                .map(AmortizationInput::getLoanAmount)
                .map(NumberSupplier::getNumber)
                .map(NumberValue::doubleValueExact)
                .flatMap(c ->
                        Optional.ofNullable(amortizationInput)
                                .map(AmortizationInput::getMonthlyInterestRate)
                                .map(BigDecimal::doubleValue)
                                .flatMap(m ->
                                        Optional.ofNullable(amortizationInput)
                                                .map(AmortizationInput::getLoanTerm)
                                                .map(d -> Math.pow(1.00 + m, d))
                                                .map(p -> p / (p - 1))
                                                .map(p -> c * m * p)
                                                .map(Object::toString)
                                                .map(AmountMapper::map)
                                                .map(amortizationInput::setMonthlyAmount)))
                .orElseThrow(() -> throwErrorWhenInputMissing("AmortizationInput"));

    }
/*
    private void checkInitialValues(HashMap<String,Object> input){
        input.entrySet().stream().map(k->Optional.ofNullable(k.getValue())
                .orElseThrow(()->throwErrorWhenInputMissing(k.getKey()));
    }

    */

    public LoanAmortization processAmortization(AmortizationInput amortizationInput, MonetaryAmount initialAmount, int period) {
        return Optional.ofNullable(initialAmount)
                .flatMap(i -> Optional.ofNullable(amortizationInput).map(p ->
                        new LoanAmortization()
                                .setPeriod(period)
                                .setInitialAmount(i)
                                .setInterest(AmountMapper.amountRounded(
                                        i.multiply(p.getMonthlyInterestRate()), 2)))
                        .map(p -> p.setRefundCapital(
                                amortizationInput.getMonthlyAmount().subtract(p.getInterest())))
                        .map(p -> p.setFinalAmount(
                                p.getInitialAmount().subtract(p.getRefundCapital()))))
                .orElseThrow(() -> throwErrorWhenInputMissing("Amortization"));
    }

    private List<LoanAmortization> processAmortizationList(AmortizationInput amortizationInput) {
        List<LoanAmortization> loanAmortizations = Stream.of(processAmortization(amortizationInput, amortizationInput.getLoanAmount(), 1))
                .collect(Collectors.toList());
        //input is mutable;
        /*
        IntStream.range(1, amortizationInput.getLoanTerm()).forEach(p -> loanAmortizations.add(
                processAmortization(amortizationInput
                        .setLoanAmount(loanAmortizations.get(p-1)
                                .getFinalAmount()), p + 1))
        );
        */
        IntStream.range(1, amortizationInput.getLoanTerm()).forEach(p -> loanAmortizations.add(
                processAmortization(amortizationInput, loanAmortizations.get(p - 1).getFinalAmount(), p + 1))
        );
        return loanAmortizations;
    }
}
