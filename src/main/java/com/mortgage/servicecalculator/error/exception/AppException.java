package com.mortgage.servicecalculator.error.exception;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * APP general exception
 */
public class AppException extends RuntimeException {

    private final ExceptionCode exceptionCode;

    public AppException(ExceptionCode exceptionCode) {
        this(null, null, exceptionCode);
    }

    public AppException(String message, ExceptionCode exceptionCode) {
        this(message, null, exceptionCode);
    }

    public AppException(String message, Throwable cause, ExceptionCode exceptionCode) {
        super(message, cause);
        this.exceptionCode = exceptionCode;
    }

    public static AppException wrap(Throwable exception) {
        return wrap(exception, TechnicalCode.API_ERROR_INTERNAL);
    }

    public static AppException wrap(Throwable exception, ExceptionCode exceptionCode) {
        if (exception instanceof AppException) {
            return (AppException) exception;
        }
        return new AppException(exception.getMessage(), exception.getCause(), exceptionCode);
    }

    public ExceptionCode getExceptionCode() {
        return exceptionCode;
    }

    @Override
    public void printStackTrace(PrintStream s) {
        printStackTrace(new PrintWriter(s));
    }

    @Override
    public void printStackTrace(PrintWriter s) {
        s.println(this);
        s.println("\t-------------------------------");
        if (exceptionCode != null) {
            s.println("\t" + exceptionCode + ":" + exceptionCode.getClass().getName());
        }
        s.println("\t-------------------------------");
        StackTraceElement[] trace = getStackTrace();
        for (StackTraceElement aTrace : trace) {
            s.println("\tat " + aTrace);
        }
        Throwable ourCause = getCause();
        if (ourCause != null) {
            ourCause.printStackTrace(s);
        }
        s.flush();
    }
}
