package com.mortgage.servicecalculator.error.exception;

import java.io.Serializable;

/**
 * Interface for different exception types
 */
public interface ExceptionCode extends Serializable {

    /**
     * HTTP status that should be returned by REST API
     *
     * @return HTTP status code
     */
    int getStatus();

    /**
     * Technical error message associated to exception code
     *
     * @return error message
     */
    String getMessage();

}
