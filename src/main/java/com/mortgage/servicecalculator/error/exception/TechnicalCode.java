package com.mortgage.servicecalculator.error.exception;

/**
 * Technical exception codes
 */
public enum TechnicalCode implements ExceptionCode {
    API_VALIDATION(400, "Invalid request payload"),
    API_MISSING_PARAMETER(400, "Mandatory request parameter is missing"),
    API_BAD_REQUEST(400, "Invalid request syntax"),
    API_ID_NOT_FOUND(400, "Path identifier could be found"),
    PAGE_NOT_FOUND(400, "Requested page not found"),
    API_UNAUTHORIZED(401, "Request not authenticated due to missing, invalid, or expired OAuth token"),
    API_FORBIDDEN(403,
                  "Client does not have sufficient permission. This can happen because the OAuth token does not have the right scopes, " +
                  "the client doesn't have permission, or the API has not been enabled for the client project"),
    API_NOT_FOUND(404, "A specified resource is not found, or the request is rejected by undisclosed reasons, such as whitelisting"),
    API_ALREADY_EXIST(409, "The resource that a client tried to create already exists"),
    API_ABORTED(409, "Concurrency conflict, such as read-modify-write conflict"),
    API_RESOURCE_EXHAUSTED(429,
                           "Either out of resource quota or reaching rate limiting. The client should look for error detail for more " +
                           "information"),
    API_ERROR_INTERNAL(500, "Internal server error. Typically a server bug."),
    API_UNKNOWN(500, "Unknown server error. Typically a server bug."),
    API_DATA_LOSS(500, "Unrecoverable data loss or data corruption. The client should report the error to the user."),
    API_DEADLINE_EXCEEDED(504, "Request deadline exceeded. If it happens repeatedly, consider reducing the request complexity.");

    private final int status;

    private final String message;

    TechnicalCode(int status, String message) {
        this.status = status;
        this.message = message;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
