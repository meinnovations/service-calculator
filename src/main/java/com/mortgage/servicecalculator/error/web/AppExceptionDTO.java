package com.mortgage.servicecalculator.error.web;

import javax.validation.constraints.NotNull;

public class AppExceptionDTO {

    private String internalMessage;

    @NotNull
    private ExceptionCodeDTO error;

    public String getInternalMessage() {
        return internalMessage;
    }

    public AppExceptionDTO setInternalMessage(String internalMessage) {
        this.internalMessage = internalMessage;
        return this;
    }

    public ExceptionCodeDTO getError() {
        return error;
    }

    public AppExceptionDTO setError(ExceptionCodeDTO error) {
        this.error = error;
        return this;
    }
}
