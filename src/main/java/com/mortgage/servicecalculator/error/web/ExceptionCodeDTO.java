package com.mortgage.servicecalculator.error.web;

import com.mortgage.servicecalculator.error.exception.ExceptionCode;

/**
 * DTO mapping {@link ExceptionCode} fields. Used for serialization and deserialization purposes
 *
 * @see ExceptionCode
 */
public class ExceptionCodeDTO implements ExceptionCode {

    private String code;

    private String message;

    private int status;

    public String getCode() {
        return code;
    }

    public ExceptionCodeDTO setCode(String code) {
        this.code = code;
        return this;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public ExceptionCodeDTO setMessage(String message) {
        this.message = message;
        return this;
    }

    public ExceptionCodeDTO setStatus(int status) {
        this.status = status;
        return this;
    }

    @Override
    public String toString() {
        return code;
    }
}
