package com.mortgage.servicecalculator.error.web;

import com.mortgage.servicecalculator.error.exception.AppException;
import com.mortgage.servicecalculator.error.exception.ExceptionCode;

public final class AppExceptionMapper {

    private AppExceptionMapper() {
    }

    public static AppExceptionDTO map(AppException exception) {
        ExceptionCode code = exception.getExceptionCode();
        return new AppExceptionDTO()
                .setInternalMessage(exception.getMessage())
                .setError(new ExceptionCodeDTO().setCode(code.toString()).setMessage(code.getMessage()).setStatus(code.getStatus()));
    }

}
