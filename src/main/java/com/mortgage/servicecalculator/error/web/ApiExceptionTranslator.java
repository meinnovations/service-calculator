package com.mortgage.servicecalculator.error.web;

import com.mortgage.servicecalculator.constants.GlobalConstants;
import com.mortgage.servicecalculator.error.exception.AppException;
import com.mortgage.servicecalculator.error.exception.ExceptionCode;
import com.mortgage.servicecalculator.error.exception.TechnicalCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import static com.mortgage.servicecalculator.error.exception.TechnicalCode.API_MISSING_PARAMETER;

/**
 * Intercept exceptions thrown by resources and try to resolved them to managed exceptions
 *
 * @see ControllerAdvice
 */
@Component
@ControllerAdvice(GlobalConstants.BASE_PACKAGE)
public class ApiExceptionTranslator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiExceptionTranslator.class);

    @ResponseBody
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<AppExceptionDTO> handle(MissingServletRequestParameterException exception) {
        TechnicalCode exceptionCode = API_MISSING_PARAMETER;
        AppExceptionDTO dto = createExceptionDTO(exception, exceptionCode);
        logError(exception);
        return new ResponseEntity<>(dto, HttpStatus.valueOf(exceptionCode.getStatus()));
    }

    @ResponseBody
    @ExceptionHandler(AppException.class)
    public ResponseEntity<AppExceptionDTO> handle(AppException e) {
        AppExceptionDTO dto = AppExceptionMapper.map(e);
        logError(e);
        return new ResponseEntity<>(dto, HttpStatus.valueOf(e.getExceptionCode().getStatus()));
    }

    private AppExceptionDTO createExceptionDTO(Exception exception, ExceptionCode code) {
        return new AppExceptionDTO()
                .setError(new ExceptionCodeDTO().setCode(code.toString())
                        .setStatus(code.getStatus())
                        .setMessage(code.getMessage()))
                .setInternalMessage(exception.getMessage());
    }

    private void logError(Throwable exception) {
        LOGGER.error(String.format("An error occured in application %s", exception));
    }

}
