package com.mortgage.servicecalculator.mapper;

import com.mortgage.servicecalculator.model.api.AmortizationInputDTO;
import com.mortgage.servicecalculator.model.bo.AmortizationInput;

import java.util.Optional;

public final class AmortizationInputMapper {

    private AmortizationInputMapper() {
    }

    public static AmortizationInput map(AmortizationInputDTO input) {
        return new AmortizationInput()
                .setAnnualInterestRate(NumberMapper.mapIBigDecimal(input.getAnnualInterestRate()))
                .setMonthlyAmount(AmountMapper.map(input.getMonthlyAmount()))
                .setLoanAmount(AmountMapper.map(input.getLoanAmount()))
                .setLoanTerm(NumberMapper.mapInteger(input.getLoanTerm()))
                .setMonthlyInterestRate(NumberMapper.mapIBigDecimal(input.getAnnualInterestRate()));
    }

    public static AmortizationInput map(String loanAmount, String loanTerm, String annualInterestRate) {
        AmortizationInputDTO input = new AmortizationInputDTO()
                .setAnnualInterestRate(NumberMapper.mapString(annualInterestRate))
                .setLoanTerm(NumberMapper.mapString(loanTerm))
                .setLoanAmount(NumberMapper.mapString(loanAmount));
        return new AmortizationInput()
                .setAnnualInterestRate(NumberMapper.mapIBigDecimal(input.getAnnualInterestRate()))
                .setMonthlyAmount(AmountMapper.map(input.getMonthlyAmount()))
                .setLoanAmount(AmountMapper.map(input.getLoanAmount()))
                .setLoanTerm(NumberMapper.mapInteger(input.getLoanTerm()))
                .setMonthlyInterestRate(NumberMapper.mapIBigDecimal(input.getMonthlyInterestRate()));
    }


    public static AmortizationInputDTO map(AmortizationInput input) {
        return new AmortizationInputDTO()
                .setAnnualInterestRate(NumberMapper.mapString(input.getAnnualInterestRate()))
                .setMonthlyAmount(AmountMapper.map(input.getMonthlyAmount()))
                .setLoanAmount(AmountMapper.map(input.getLoanAmount()))
                .setLoanTerm(NumberMapper.mapString(input.getLoanTerm()))
                .setMonthlyInterestRate(NumberMapper.mapString(input.getMonthlyInterestRate()));
    }
}