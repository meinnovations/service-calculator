package com.mortgage.servicecalculator.mapper;

import com.mortgage.servicecalculator.model.api.LoanDTO;
import com.mortgage.servicecalculator.model.bo.Loan;

import javax.money.MonetaryAmount;
import java.util.Optional;

import static com.mortgage.servicecalculator.constants.GlobalConstants.*;

public final class LoanMapper {

    private LoanMapper() {
    }

    public static Loan map(LoanDTO input) {
        return new Loan()
                .setHomeValue(constraintHomeValueAndCharge(input))
                .setInitialCapital(AmountMapper.map(input.getInitialCapital()))
                .setLoanAmount(constraintFeeAndLoan(input));
    }

    public static Loan map(String homeValue, String purchaseCharge, String initialCapital,
                           String mortgageFee, String loanAmount) {
        LoanDTO input = new LoanDTO().setMortgageFee(NumberMapper.mapString(mortgageFee))
                .setHomeValue(NumberMapper.mapString(homeValue))
                .setInitialCapital(NumberMapper.mapString(initialCapital))
                .setLoanAmount(NumberMapper.mapString(loanAmount))
                .setPurchaseCharge(NumberMapper.mapString(purchaseCharge));
        return new Loan()
                .setHomeValue(constraintHomeValueAndCharge(input))
                .setInitialCapital(AmountMapper.map(input.getInitialCapital()))
                .setLoanAmount(constraintFeeAndLoan(input));
    }

    public static LoanDTO map(Loan input) {
        return new LoanDTO()
                .setHomeValue(AmountMapper.map(input.getHomeValue()))
                .setPurchaseCharge(constraintHomeValueAndCharge(input))
                .setInitialCapital(AmountMapper.map(input.getInitialCapital()))
                .setLoanAmount(AmountMapper.map(input.getLoanAmount()))
                .setMortgageFee(constraintFeeAndLoan(input));
    }

    private static MonetaryAmount constraintFeeAndLoan(LoanDTO input) {
        return Optional.ofNullable(input.getLoanAmount()).map(AmountMapper::map).orElse(
                Optional.ofNullable(input.getMortgageFee())
                        .map(AmountMapper::map)
                        .map(p -> p.divide(FEE_RATE)).orElse(null)
        );
    }

    private static MonetaryAmount constraintHomeValueAndCharge(LoanDTO input) {
        return Optional.ofNullable(input.getHomeValue()).map(AmountMapper::map).orElse(
                Optional.ofNullable(input.getPurchaseCharge())
                        .map(AmountMapper::map)
                        .map(p -> p.divide(CHARGE_RATE))
                        .map(p -> p.add(AmountMapper.map(CHARGE_LEVEL)))
                        .orElse(null)
        );
    }

    private static String constraintFeeAndLoan(Loan input) {
        return Optional.ofNullable(input.getLoanAmount())
                .map(p -> p.multiply(FEE_RATE))
                .map(AmountMapper::map).orElse(null);
    }

    private static String constraintHomeValueAndCharge(Loan input) {
        return Optional.ofNullable(input.getHomeValue())
                .map(p -> p.subtract(AmountMapper.map(CHARGE_LEVEL)))
                .map(p -> p.multiply(CHARGE_RATE))
                .map(AmountMapper::map).orElse(null);
    }
}
