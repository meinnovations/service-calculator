package com.mortgage.servicecalculator.mapper;

import com.mortgage.servicecalculator.error.exception.AppException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Optional;

import static com.mortgage.servicecalculator.error.exception.TechnicalCode.API_BAD_REQUEST;
import static java.lang.String.format;

public final class NumberMapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(String.class);

    private NumberMapper() {
    }

    public static Integer mapInteger(String input) {
        return Optional.ofNullable(input)
                .filter(p -> !p.isEmpty())
                .map(NumberMapper::isStringAInteger)
                .map(Integer::parseInt).orElse(null);
        /*
        try {
            return Optional.of(input).map(Integer::parseInt).orElse(null);
        } catch (Exception e) {
            throw throwErrorWhenStringIsNotInteger(input);
        }
       */
    }

    public static String isStringAInteger(String input) {
        return Optional.ofNullable(input)
                .filter(p -> p.matches("[0-9]+"))
                .filter(p -> !p.matches("0[0-9]+"))
                .orElseThrow(() -> throwErrorWhenStringIsNotInteger(input));
    }

    public static String isStringANumber(String input) {
        return Optional.ofNullable(input)
                .filter(p -> p.matches("[0-9]+[.][0-9]+|[0-9]+"))
                .filter(p -> !p.matches("0[0-9]+[.][0-9]+|0[0-9]+"))
                .orElseThrow(() -> throwErrorWhenStringIsNotNumber(input));
    }

    private static AppException throwErrorWhenStringIsNotInteger(String input) {
        String errorMsg = format("The input %s should be an integer", input);
        LOGGER.debug(errorMsg);
        return new AppException(errorMsg, API_BAD_REQUEST);
    }

    private static AppException throwErrorWhenStringIsNotNumber(String input) {
        String errorMsg = format("The input %s should be a number", input);
        LOGGER.debug(errorMsg);
        return new AppException(errorMsg, API_BAD_REQUEST);
    }

    public static <T> String mapString(T input) {
        return Optional.ofNullable(input).map(Object::toString).orElse(null);
    }

    public static BigDecimal mapIBigDecimal(String input) {
        return Optional.ofNullable(input).map(p -> new BigDecimal(input)).orElse(null);
    }

    public static double mapDoubleToBigDecimal(BigDecimal input) {
        return input.doubleValue();
    }

    public static BigDecimal mapDoubleToBigDecimal(double input, int round) {
        return Optional.of(input).map(BigDecimal::new)
                .map(p -> roundBigDecimal(p,round)).orElse(null);
    }

    public static BigDecimal roundBigDecimal(BigDecimal input, int round) {
        return Optional.of(input)
                .map(p -> p.setScale(round, RoundingMode.HALF_UP)).orElse(null);
    }
}
