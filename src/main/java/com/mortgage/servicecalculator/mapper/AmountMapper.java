package com.mortgage.servicecalculator.mapper;

import org.javamoney.moneta.Money;

import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;
import javax.money.NumberSupplier;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Optional;

/**
 * Mapping related to {@link MonetaryAmount}
 *
 * @author jpiraguha
 */
public final class AmountMapper {

    private AmountMapper() {
    }

    public static String map(MonetaryAmount input) {
        return Optional.ofNullable(input).map(AmountMapper::serializeAmount).orElse(null);
    }

    public static MonetaryAmount map(String input) {
        return Optional.ofNullable(input)
                .filter(p -> !p.isEmpty())
                .map(NumberMapper::isStringANumber)
                .map(p -> Money.of(
                        NumberMapper.roundBigDecimal(new BigDecimal(p), 2), "EUR"))
                .orElse(null);
    }

    public static MonetaryAmount amountRounded (MonetaryAmount input, int round) {
        return Optional.ofNullable(input)
                .map(NumberSupplier::getNumber)
                .map(Object::toString)
                .map(NumberMapper::isStringANumber)
                .map(p -> Money.of(
                        NumberMapper.roundBigDecimal(new BigDecimal(p), 2), "EUR"))
                .orElse(null);
    }

    private static String serializeAmount(MonetaryAmount input) {
        BigDecimal amount = input.getNumber().numberValueExact(BigDecimal.class);
        CurrencyUnit currency = input.getCurrency();
        DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(Locale.ENGLISH);
        decimalFormat.applyPattern("###.##");
        decimalFormat.setMaximumFractionDigits(currency.getDefaultFractionDigits());
        decimalFormat.setMinimumFractionDigits(currency.getDefaultFractionDigits());
        return decimalFormat.format(amount);
    }
}
