package com.mortgage.servicecalculator.mapper;

import com.mortgage.servicecalculator.model.api.AmortizationInputDTO;
import com.mortgage.servicecalculator.model.api.AmortizationTableDTO;
import com.mortgage.servicecalculator.model.api.LoanAmortizationDTO;
import com.mortgage.servicecalculator.model.bo.AmortizationTable;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class AmortizationTableMapper {

    private AmortizationTableMapper() {
    }

    public static AmortizationTableDTO map(AmortizationTable input) {
        return new AmortizationTableDTO()
                .setAmortizationInput(mapAmortizationInputIfExist(input))
                .setAmortizations(mapsListOfAmortizations(input));
    }

    private static AmortizationInputDTO mapAmortizationInputIfExist(AmortizationTable input) {
        return Optional.ofNullable(input.getAmortizationInput())
                .map(AmortizationInputMapper::map).orElse(null);
    }

    private static List<LoanAmortizationDTO> mapsListOfAmortizations(AmortizationTable input) {
        return Optional.ofNullable(input)
                .map(p -> p.getAmortizations().stream()
                        .map(LoanAmortizationMapper::map)
                        .collect(Collectors.toList())).orElse(null);
    }

}
