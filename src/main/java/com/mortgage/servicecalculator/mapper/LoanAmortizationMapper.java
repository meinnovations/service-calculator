package com.mortgage.servicecalculator.mapper;

import com.mortgage.servicecalculator.model.api.LoanAmortizationDTO;
import com.mortgage.servicecalculator.model.bo.LoanAmortization;

public final class LoanAmortizationMapper {

    private LoanAmortizationMapper() {
    }

    public static LoanAmortization map(LoanAmortizationDTO input) {
        return new LoanAmortization()
                .setPeriod(NumberMapper.mapInteger(input.getPeriod()))
                .setInitialAmount(AmountMapper.map(input.getInitialAmount()))
                .setFinalAmount(AmountMapper.map(input.getFinalAmount()))
                .setInterest(AmountMapper.map(input.getInterest()))
                .setRefundCapital(AmountMapper.map(input.getRefundCapital()));

    }

    public static LoanAmortizationDTO map(LoanAmortization input) {
        return new LoanAmortizationDTO()
                .setPeriod(NumberMapper.mapString(input.getPeriod()))
                .setInitialAmount(AmountMapper.map(input.getInitialAmount()))
                .setFinalAmount(AmountMapper.map(input.getFinalAmount()))
                .setInterest(AmountMapper.map(input.getInterest()))
                .setRefundCapital(AmountMapper.map(input.getRefundCapital()));
    }
}
