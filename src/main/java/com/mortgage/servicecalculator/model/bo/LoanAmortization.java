package com.mortgage.servicecalculator.model.bo;

import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.util.Objects;

public class LoanAmortization {

    int period;

    MonetaryAmount initialAmount;

    MonetaryAmount interest;

    MonetaryAmount refundCapital;

    MonetaryAmount finalAmount;

    public int getPeriod() {
        return period;
    }

    public LoanAmortization setPeriod(int period) {
        this.period = period;
        return this;
    }

    public MonetaryAmount getInitialAmount() {
        return initialAmount;
    }

    public LoanAmortization setInitialAmount(MonetaryAmount initialAmount) {
        this.initialAmount = initialAmount;
        return this;
    }

    public MonetaryAmount getInterest() {
        return interest;
    }

    public LoanAmortization setInterest(MonetaryAmount interest) {
        this.interest = interest;
        return this;
    }

    public MonetaryAmount getRefundCapital() {
        return refundCapital;
    }

    public LoanAmortization setRefundCapital(MonetaryAmount refundCapital) {
        this.refundCapital = refundCapital;
        return this;
    }

    public MonetaryAmount getFinalAmount() {
        return finalAmount;
    }

    public LoanAmortization setFinalAmount(MonetaryAmount finalAmount) {
        this.finalAmount = finalAmount;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LoanAmortization)) return false;
        LoanAmortization that = (LoanAmortization) o;
        return period == that.period &&
                Objects.equals(initialAmount, that.initialAmount) &&
                Objects.equals(interest, that.interest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(period, initialAmount, interest);
    }
}
