package com.mortgage.servicecalculator.model.bo;

import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.util.Objects;

public class AmortizationInput {

    MonetaryAmount loanAmount;

    int loanTerm;

    BigDecimal annualInterestRate;

    BigDecimal monthlyInterestRate;

    MonetaryAmount monthlyAmount;

    public MonetaryAmount getLoanAmount() {
        return loanAmount;
    }

    public AmortizationInput setLoanAmount(MonetaryAmount loanAmount) {
        this.loanAmount = loanAmount;
        return this;
    }

    public int getLoanTerm() {
        return loanTerm;
    }

    public AmortizationInput setLoanTerm(int loanTerm) {
        this.loanTerm = loanTerm;
        return this;
    }

    public BigDecimal getAnnualInterestRate() {
        return annualInterestRate;
    }

    public AmortizationInput setAnnualInterestRate(BigDecimal annualInterestRate) {
        this.annualInterestRate = annualInterestRate;
        return this;
    }

    public BigDecimal getMonthlyInterestRate() {
        return monthlyInterestRate;
    }

    public AmortizationInput setMonthlyInterestRate(BigDecimal monthlyInterestRate) {
        this.monthlyInterestRate = monthlyInterestRate;
        return this;
    }

    public MonetaryAmount getMonthlyAmount() {
        return monthlyAmount;
    }

    public AmortizationInput setMonthlyAmount(MonetaryAmount monthlyAmount) {
        this.monthlyAmount = monthlyAmount;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AmortizationInput)) return false;
        AmortizationInput input = (AmortizationInput) o;
        return loanTerm == input.loanTerm &&
                Objects.equals(loanAmount, input.loanAmount) &&
                Objects.equals(annualInterestRate, input.annualInterestRate) &&
                Objects.equals(monthlyInterestRate, input.monthlyInterestRate) &&
                Objects.equals(monthlyAmount, input.monthlyAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(loanAmount, loanTerm, annualInterestRate, monthlyInterestRate, monthlyAmount);
    }
}
