package com.mortgage.servicecalculator.model.bo;

import javax.money.MonetaryAmount;

public class Loan {

    MonetaryAmount homeValue;

    MonetaryAmount initialCapital;

    MonetaryAmount loanAmount;

    public MonetaryAmount getHomeValue() {
        return homeValue;
    }

    public Loan setHomeValue(MonetaryAmount homeValue) {
        this.homeValue = homeValue;
        return this;
    }

    public MonetaryAmount getInitialCapital() {
        return initialCapital;
    }

    public Loan setInitialCapital(MonetaryAmount initialCapital) {
        this.initialCapital = initialCapital;
        return this;
    }

    public MonetaryAmount getLoanAmount() {
        return loanAmount;
    }

    public Loan setLoanAmount(MonetaryAmount loanAmount) {
        this.loanAmount = loanAmount;
        return this;
    }
}
