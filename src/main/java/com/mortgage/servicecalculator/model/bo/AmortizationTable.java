package com.mortgage.servicecalculator.model.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AmortizationTable {

    AmortizationInput amortizationInput;

    List<LoanAmortization> amortizations = new ArrayList<>();

    public AmortizationInput getAmortizationInput() {
        return amortizationInput;
    }

    public AmortizationTable setAmortizationInput(AmortizationInput amortizationInput) {
        this.amortizationInput = amortizationInput;
        return this;
    }

    public List<LoanAmortization> getAmortizations() {
        return amortizations;
    }

    public AmortizationTable setAmortizations(List<LoanAmortization> amortizations) {
        this.amortizations = amortizations;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AmortizationTable)) return false;
        AmortizationTable that = (AmortizationTable) o;
        return Objects.equals(amortizationInput, that.amortizationInput);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amortizationInput);
    }
}
