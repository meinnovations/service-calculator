package com.mortgage.servicecalculator.model.api;

import java.util.ArrayList;
import java.util.List;

public class AmortizationTableDTO {

    AmortizationInputDTO amortizationInput;

    List<LoanAmortizationDTO> amortizations = new ArrayList<>();

    public AmortizationInputDTO getAmortizationInput() {
        return amortizationInput;
    }

    public AmortizationTableDTO setAmortizationInput(AmortizationInputDTO amortizationInput) {
        this.amortizationInput = amortizationInput;
        return this;
    }

    public List<LoanAmortizationDTO> getAmortizations() {
        return amortizations;
    }

    public AmortizationTableDTO setAmortizations(List<LoanAmortizationDTO> amortizations) {
        this.amortizations = amortizations;
        return this;
    }
}
