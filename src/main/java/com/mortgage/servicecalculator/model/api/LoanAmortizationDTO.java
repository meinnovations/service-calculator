package com.mortgage.servicecalculator.model.api;

public class LoanAmortizationDTO {

    String period;

    String initialAmount;

    String interest;

    String refundCapital;

    String finalAmount;

    public String getPeriod() {
        return period;
    }

    public LoanAmortizationDTO setPeriod(String period) {
        this.period = period;
        return this;
    }

    public String getInitialAmount() {
        return initialAmount;
    }

    public LoanAmortizationDTO setInitialAmount(String initialAmount) {
        this.initialAmount = initialAmount;
        return this;
    }

    public String getInterest() {
        return interest;
    }

    public LoanAmortizationDTO setInterest(String interest) {
        this.interest = interest;
        return this;
    }

    public String getRefundCapital() {
        return refundCapital;
    }

    public LoanAmortizationDTO setRefundCapital(String refundCapital) {
        this.refundCapital = refundCapital;
        return this;
    }

    public String getFinalAmount() {
        return finalAmount;
    }

    public LoanAmortizationDTO setFinalAmount(String finalAmount) {
        this.finalAmount = finalAmount;
        return this;
    }
}
