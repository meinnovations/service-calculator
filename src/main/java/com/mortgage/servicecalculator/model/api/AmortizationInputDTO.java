package com.mortgage.servicecalculator.model.api;

public class AmortizationInputDTO {

    String loanAmount;

    String loanTerm;

    String annualInterestRate;

    String monthlyInterestRate;

    String monthlyAmount;

    public String getLoanAmount() {
        return loanAmount;
    }

    public AmortizationInputDTO setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
        return this;
    }

    public String getLoanTerm() {
        return loanTerm;
    }

    public AmortizationInputDTO setLoanTerm(String loanTerm) {
        this.loanTerm = loanTerm;
        return this;
    }

    public String getAnnualInterestRate() {
        return annualInterestRate;
    }

    public AmortizationInputDTO setAnnualInterestRate(String annualInterestRate) {
        this.annualInterestRate = annualInterestRate;
        return this;
    }

    public String getMonthlyInterestRate() {
        return monthlyInterestRate;
    }

    public AmortizationInputDTO setMonthlyInterestRate(String monthlyInterestRate) {
        this.monthlyInterestRate = monthlyInterestRate;
        return this;
    }

    public String getMonthlyAmount() {
        return monthlyAmount;
    }

    public AmortizationInputDTO setMonthlyAmount(String monthlyAmount) {
        this.monthlyAmount = monthlyAmount;
        return this;
    }
}
