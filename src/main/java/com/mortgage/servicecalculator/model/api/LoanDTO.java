package com.mortgage.servicecalculator.model.api;

public class LoanDTO {

    String homeValue;

    String purchaseCharge;

    String initialCapital;

    String mortgageFee;

    String loanAmount;

    public String getHomeValue() {
        return homeValue;
    }

    public LoanDTO setHomeValue(String homeValue) {
        this.homeValue = homeValue;
        return this;
    }

    public String getPurchaseCharge() {
        return purchaseCharge;
    }

    public LoanDTO setPurchaseCharge(String purchaseCharge) {
        this.purchaseCharge = purchaseCharge;
        return this;
    }

    public String getInitialCapital() {
        return initialCapital;
    }

    public LoanDTO setInitialCapital(String initialCapital) {
        this.initialCapital = initialCapital;
        return this;
    }

    public String getMortgageFee() {
        return mortgageFee;
    }

    public LoanDTO setMortgageFee(String mortgageFee) {
        this.mortgageFee = mortgageFee;
        return this;
    }

    public String getLoanAmount() {
        return loanAmount;
    }

    public LoanDTO setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
        return this;
    }
}
