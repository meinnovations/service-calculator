package com.mortgage.servicecalculator.resource;

import com.mortgage.servicecalculator.mapper.AmortizationInputMapper;
import com.mortgage.servicecalculator.mapper.AmortizationTableMapper;
import com.mortgage.servicecalculator.model.api.AmortizationTableDTO;
import com.mortgage.servicecalculator.sercice.AmortizationService;
import io.reactivex.Single;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import static com.mortgage.servicecalculator.constants.GlobalConstants.AMORTIZATION;

@RestController
@RequestMapping(AMORTIZATION)
public class AmortizationResource {

    private final AmortizationService amortizationService;

    public AmortizationResource(AmortizationService amortizationService) {
        this.amortizationService = amortizationService;
    }

    @GetMapping
    public Single<AmortizationTableDTO> calculate (@RequestParam String loanAmount,
                                                   @RequestParam String loanTerm,
                                                   @RequestParam String annualInterestRate) {
        return amortizationService.calculateLoanAmortization(AmortizationInputMapper.map(loanAmount, loanTerm, annualInterestRate))
                .map(AmortizationTableMapper::map);
    }
}
