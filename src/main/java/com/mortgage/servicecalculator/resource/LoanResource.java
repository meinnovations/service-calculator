package com.mortgage.servicecalculator.resource;

import com.mortgage.servicecalculator.mapper.LoanMapper;
import com.mortgage.servicecalculator.model.api.LoanDTO;
import com.mortgage.servicecalculator.sercice.LoanService;
import io.reactivex.Single;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.mortgage.servicecalculator.constants.GlobalConstants.LOAN;

@RestController
@RequestMapping(LOAN)
public class LoanResource {

    private final LoanService loanService;

    public LoanResource(LoanService loanService) {
        this.loanService = loanService;
    }

    @GetMapping
    public Single<LoanDTO> calculate(@RequestParam(required = false) String homeValue,
                                     @RequestParam(required = false) String purchaseCharge,
                                     @RequestParam(required = false) String initialCapital,
                                     @RequestParam(required = false) String mortgageFee,
                                     @RequestParam(required = false) String loanAmount) {
        return loanService.calculateLoan(LoanMapper.map(homeValue,purchaseCharge,initialCapital,mortgageFee,loanAmount))
                .map(LoanMapper::map);
    }
}
