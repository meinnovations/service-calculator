package com.mortgage.servicecalculator.constants;

/**
 * Global constants
 */
public final class GlobalConstants {

    public static final String BASE_PACKAGE = "com.mortgage";

    public static final String API = "/api";

    public static final String LOAN = API + "/loans";

    public static final String AMORTIZATION = API + "/amortizations";

    public static double FEE_RATE = 0.02;

    public static double CHARGE_RATE = 0.1;

    public static String CHARGE_LEVEL = "50000";

    public GlobalConstants() {
    }
}