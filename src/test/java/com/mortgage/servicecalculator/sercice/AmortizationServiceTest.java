package com.mortgage.servicecalculator.sercice;

import com.mortgage.servicecalculator.mapper.AmountMapper;
import com.mortgage.servicecalculator.model.bo.AmortizationInput;
import com.mortgage.servicecalculator.model.bo.LoanAmortization;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AmortizationServiceTest {

    @Autowired
    private AmortizationService amortizationService;

    @Test
    public void processMonthlyAmount_shouldWork(){
        AmortizationInput input = new AmortizationInput().setMonthlyInterestRate(new BigDecimal("0.00198"))
                .setAnnualInterestRate(new BigDecimal("0.024"))
                .setLoanTerm(240)
                .setLoanAmount(AmountMapper.map("107142.86"));
        AmortizationInput output = amortizationService.processMonthlyAmount(input);
        assertThat(output.getMonthlyAmount().getNumber().doubleValueExact()).isEqualTo(561.30);
    }

    @Test
    public void processMonthlyInterestRate_shouldWork(){
        BigDecimal input=new BigDecimal("0.024");
        BigDecimal output= amortizationService.processMonthlyInterestRate(input);
        assertThat(output.toString()).isEqualTo("0.00198");
    }

    @Test
    public void processAmortization_shouldWork(){
        AmortizationInput input = new AmortizationInput().setMonthlyInterestRate(new BigDecimal("0.00198"))
                .setAnnualInterestRate(new BigDecimal("0.024"))
                .setLoanTerm(240)
                .setLoanAmount(AmountMapper.map("107142.86"))
                .setMonthlyAmount(AmountMapper.map("561.30"));
        LoanAmortization amortization = amortizationService.processAmortization(input,input.getLoanAmount(),1);

        assertThat(amortization.getInitialAmount().getNumber().toString()).isEqualTo("107142.86");
        assertThat(amortization.getInterest().getNumber().toString()).isEqualTo("212.14");
        assertThat(amortization.getRefundCapital().getNumber().toString()).isEqualTo("349.16");
        assertThat(amortization.getFinalAmount().getNumber().doubleValueExact()).isEqualTo(106793.70);
        assertThat(amortization.getPeriod()).isEqualTo(1);
    }

}