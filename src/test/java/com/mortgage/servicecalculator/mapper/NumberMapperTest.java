package com.mortgage.servicecalculator.mapper;

import com.mortgage.servicecalculator.error.exception.AppException;
import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class NumberMapperTest {

    @Test
    public void isStringAInteger_shouldWork() {
        String result = NumberMapper.isStringAInteger("0");
        assertThat(result).isEqualTo("0");
    }

    @Test
    public void isStringANumber_shouldWork() {
        String result = NumberMapper.isStringANumber("0.100");
        assertThat(result).isEqualTo("0.100");
    }

    @Test(expected = AppException.class)
    public void isStringAInteger_shouldFailCauzNotValidInt() {
        String result = NumberMapper.isStringAInteger("01");
    }

    @Test(expected = AppException.class)
    public void isStringANumber_shouldFailCauzNotValidNumber() {
        String result = NumberMapper.isStringANumber("00.100");
    }

}